package com.example.remotenotificationfirebase;

import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        super.onMessageReceived( remoteMessage );
        Log.d("Messaging Service", remoteMessage.getNotification().getBody());
      showNotification( remoteMessage.getNotification().getBody() );
        Intent intent = new Intent( getApplicationContext(), MainActivity.class );
        intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity( intent );
    }

    public void showNotification(String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder( this, "mudasir" )
                .setContentTitle( "Notification" )
                .setSmallIcon( R.drawable.ic_launcher_background )
                .setAutoCancel( true )
                .setContentText( message );

        NotificationManagerCompat manager = NotificationManagerCompat.from( this );
        manager.notify( 22, builder.build() );

    }



}


