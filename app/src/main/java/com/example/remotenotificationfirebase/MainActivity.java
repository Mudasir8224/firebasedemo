package com.example.remotenotificationfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class MainActivity extends AppCompatActivity {

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        button = findViewById( R.id.buttonNotify );

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener( new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        if (task.isSuccessful()){
                            button.setText( "token generated" );
                            Log.d("Call","Token is : "+task.getResult().getToken());

                        }else {
                            button.setText( "token not generated" );
                        }

                    }
                } );


/*
        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel =
                            new NotificationChannel( "mudasir", "kya ba", NotificationManager.IMPORTANCE_DEFAULT );
                    NotificationManager manager = getSystemService( NotificationManager.class );
                    manager.createNotificationChannel( channel );


                }


                NotificationCompat.Builder builder = new NotificationCompat.Builder( MainActivity.this, "mudasir" )
                        .setContentTitle( "Notification" )
                        .setSmallIcon( R.drawable.ic_launcher_background )
                        .setAutoCancel( true )
                        .setContentText( "hi,what's up" );

                NotificationManagerCompat manager = NotificationManagerCompat.from( MainActivity.this );
                manager.notify( 22, builder.build() );
            }
        } );




 */

    }
}
